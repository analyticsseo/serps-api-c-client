# SERPs API C# client

## Repository structure

* _SerpsApi_ - main library project
* _SerpsApi.DependencyInjection_ - dependency injection service extension project
* _SerpsApi.SampleCli_ - sample console application with an example of Serps API client usage
* _SerpsApi.SampleApp_ - sample ASP.NET Core Web API application with an example of Serps API client usage through the dependency injection
* _SerpsApi.Tests_ - project with library tests

## Usage example

Install the library packages:

```bash
dotnet add package Authoritas.SerpsApi
dotnet add package Authoritas.SerpsApi.DependencyInjection # if the DI is needed
```

### Use as a separate library

```C#
using System;
using System.Net.Http;
using Authoritas.SerpsApi.Authorization;
using Authoritas.SerpsApi.Transport;
using Authoritas.SerpsApi.V3;

...

// Create a HTTP client
var httpClient = new HttpClient();
httpClient.BaseAddress = new Uri("http://v3.api.analyticsseo.com");

// Create an authorization provider with your API credentials
var auth = new KeyAuthorizationProvider("YOUR PUBLIC KEY", "YOUR PRIVATE KEY", "YOUR SALT");

// Create the API transport
var transport = new HttpTransport(httpClient, auth);

// Create the API client
var client = new SerpsApiClient(transport);

// Submit the job request
var submittedRequest = await client.CreateJob(new SerpsRequest
{
    SearchEngine = SearchEngine.Google,
    Phrase = "How good is your SEO?",
    Region = "global",
    Language = "en",
    MaxResults = 10,
});

// Request the results (when the job should be ready)
var result = await client.GetJobResult(submittedRequest.JobId);
if (result.Ready)
{
    // The job is ready, and the search results can be accessed
    foreach (var res in result.Response.Results)
    {
        // ...
    }
}
else
{
    //  The job is not ready, try again later
}
```

Check the `SerpsApi.SampleCli` project for a more full-featured example.

### Use through the dependency injection

In the `ConfigureServices` method of your `Startup` class register the SERPs API client service:

```C#
services.AddSerpsApiClient(serps =>
{
    serps.WithHttpTransport(new Uri("http://v3.api.analyticsseo.com"));
    serps.WithKeyAuthorization(cfg.PublicKey, cfg.PrivateKey, cfg.Salt);
});
```

Then, in any controller or service, request the client API through the DI:

```C#
public class SomeService
{
    public SomeService(ISerpsApiClient serpsClient)
    {
        this.serpsClient = serpsClient;
    }

    public async void Request(string jobId)
    {
        var result = await serpsClient.GetJobResult(jobId);
        ...
    }
}
```

Check the `SerpsApi.SampleApp` project for a more full-featured example.

## Building Nuget packages

Run `dotnet build -c Release` from the repository root.

Following Nuget packages will be built:

* SerpsApi/bin/Release/Authoritas.SerpsApi.1.0.0.nupkg
* SerpsApi.DependencyInjection/bin/Release/Authoritas.SerpsApi.DependencyInjection.1.0.0.nupkg

## Launching sample CLI app

From the `SerpsApi.SampleCli` folder launch the application with the API credentials provided via environment:

```
SERPS_PUBLIC_KEY=... SERPS_PRIVATE_KEY=... SERPS_SALT=... dotnet run
```

## Launching sample ASP.NET app

* Go to `SerpsApi.SampleApp` folder.
* Copy `appsettings.json` to `appsettings.Development.json`, enter your API credentials.
* Launch the application with `dotnet watch run` command.
* Web interface for API calls testing will be available at `https://localhost:5001/swagger/index.html`.

## Launching tests with coverage report generation

First, install coverage report generation tool:

```
dotnet tool install -g dotnet-reportgenerator-globaltool
```

From `SerpsApi.Tests` folder launch `run_tests.sh` script.

The coverage report entry point will be available at generated `Coverage/index.htm` file.
