namespace Authoritas.SerpsApi.SampleApp
{
    public class SerpsApiConfiguration
    {
        public string PublicKey { get; set; }

        public string PrivateKey { get; set; }

        public string Salt { get; set; }
    }
}
