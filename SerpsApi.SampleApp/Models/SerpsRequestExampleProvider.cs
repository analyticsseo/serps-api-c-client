using Swashbuckle.AspNetCore.Filters;
using Authoritas.SerpsApi.V3;

namespace Authoritas.SerpsApi.SampleApp.Models
{
    class SerpsRequestExampleProvider : IExamplesProvider<SerpsRequest>
    {
        public SerpsRequest GetExamples()
        {
            return new SerpsRequest
            {
                SearchEngine = SearchEngine.Google,
                Phrase = "How good is my SEO?",
                Region = "global",
                Language = "en"
            };
        }
    }
}
