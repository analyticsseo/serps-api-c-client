using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Filters;
using Authoritas.SerpsApi.V3;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.SampleApp.Models
{
    class SerpsResponseExampleProvider : IExamplesProvider<SerpsResponse>
    {
        public SerpsResponse GetExamples()
        {
            var jobId = Guid.NewGuid().ToString();

            return new SerpsResponse
            {
                JobId = jobId,
                Ready = true,
                Request = new SerpsProcessedRequest
                {
                    JobId = jobId,
                    CreatedDate = DateTime.UtcNow,
                    ProcessedDate = DateTime.UtcNow,
                    SearchEngine = SearchEngine.Google,
                    Phrase = "How good is my SEO?",
                    Region = "global",
                    Language = "en"
                },
                Response = new SerpsResponseData
                {
                    Summary = new SerpsSummary
                    {
                        Global = new SerpsSummaryEntry
                        {
                            { "organic", 10 },
                            { "image", 6 },
                            { "video", 4 },
                        },
                        Pages = new Dictionary<string, SerpsSummaryEntry>
                        {
                            {
                                "1",
                                new SerpsSummaryEntry
                                {
                                    { "total", 123 },
                                    { "organic", 10 },
                                    { "image", 2 },
                                    { "video", 4 },
                                }
                            },
                        }
                    },
                    Results = new Dictionary<string, SearchResultDictionary>
                    {
                        {
                            "organic",
                            new SearchResultDictionary
                            {
                                {
                                    "1",
                                    JObject.FromObject(new
                                    {
                                        url = "https://some-site.com/some-page.html",
                                        page_number = 1,
                                        title = "Some result",
                                        type = "organic",
                                        markup = 0,
                                    }).ToObject<SearchResult>()
                                }
                            }
                        }
                    }
                }
            };
        }
    }
}
