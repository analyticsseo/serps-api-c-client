﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Filters;
using Authoritas.SerpsApi.V3;
using Authoritas.SerpsApi.SampleApp.Models;

namespace Authoritas.SerpsApi.SampleApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestApiController : ControllerBase
    {
        private readonly ILogger<TestApiController> logger;

        private ISerpsApiClient serpsClient;

        public TestApiController(ILogger<TestApiController> logger, ISerpsApiClient serpsClient)
        {
            this.logger = logger;
            this.serpsClient = serpsClient;
        }

        [HttpPost("CreateJob")]
        [SwaggerRequestExample(typeof(SerpsRequest), typeof(SerpsRequestExampleProvider))]
        [SwaggerResponseExample((int) HttpStatusCode.OK, typeof(SerpsResponseExampleProvider))]
        public async Task<ActionResult<SerpsResponse>> CreateJob([FromBody] SerpsRequest request)
        {
            var response = await serpsClient.CreateJob(request);
            return Ok(response);
        }

        [HttpGet("GetJobResult/{jobId}")]
        [SwaggerResponseExample((int) HttpStatusCode.OK, typeof(SerpsResponseExampleProvider))]
        public async Task<ActionResult<SerpsResponse>> GetJobResult(string jobId)
        {
            var response = await serpsClient.GetJobResult(jobId);
            return Ok(response);
        }
    }
}
