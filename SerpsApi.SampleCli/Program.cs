﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Authoritas.SerpsApi.Authorization;
using Authoritas.SerpsApi.Transport;
using Authoritas.SerpsApi.V3;

using static System.Console;

namespace Authoritas.SerpsApi.SampleCli
{
    class Program
    {
        /**
         * To launch the CLI sample, API credentials should be provided via the environment:
         *
         * SERPS_PUBLIC_KEY=... SERPS_PRIVATE_KEY=... SERPS_SALT=... dotnet run
         */
        static async Task Main(string[] args)
        {
            // Create a HTTP client
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://v3.api.analyticsseo.com");

            // Get credentials from the environment
            var publicKey = Environment.GetEnvironmentVariable("SERPS_PUBLIC_KEY");
            var privateKey = Environment.GetEnvironmentVariable("SERPS_PRIVATE_KEY");
            var salt = Environment.GetEnvironmentVariable("SERPS_SALT");

            // Create an authorization provider with your API credentials
            var auth = new KeyAuthorizationProvider(publicKey, privateKey, salt);

            // Create the API transport
            var transport = new HttpTransport(httpClient, auth);

            // Create the API client
            var client = new SerpsApiClient(transport);

            // Prepare the request
            var request = new SerpsRequest
            {
                SearchEngine = SearchEngine.Google,
                Phrase = "How good is your SEO?",
                Region = "global",
                Language = "en",
                MaxResults = 100,
            };

            WriteLine($"Submitting request: {JsonConvert.SerializeObject(request, Formatting.Indented)}");

            // Submit the job request
            var submittedRequest = await client.CreateJob(request);
            if (submittedRequest.Error != null)
            {
                throw new Exception(submittedRequest.Error);
            }

            // Request the results
            SerpsResponse response = null;
            var sleepDuration = 10.0;
            while (response == null || !response.Ready)
            {
                Console.WriteLine($"Waiting for {sleepDuration} seconds before fetching the job results...");
                Thread.Sleep(TimeSpan.FromSeconds(sleepDuration));
                response = await client.GetJobResult(submittedRequest.JobId);
                if (response.Error != null)
                {
                    throw new Exception(submittedRequest.Error);
                }
            }

            WriteLine($"Got response for jobId {response.JobId}");

            // Outputting Summary section
            if (response.Response.Summary != null)
            {
                WriteLine($"Summary global: ");
                DumpSerpsSummary(response.Response.Summary.Global);
                foreach (var summaryPage in response.Response.Summary.Pages)
                {
                    WriteLine($"Summary page {summaryPage.Key}: ");
                    DumpSerpsSummary(summaryPage.Value);
                }
            }

            // Outputting search results
            if (response.Response.Results != null)
            {
                foreach (var resultsByType in response.Response.Results)
                {
                    if (resultsByType.Value.Count > 0)
                    {
                        WriteLine($"Search results of type {resultsByType.Key}:");
                    }

                    foreach (var result in resultsByType.Value)
                    {
                        WriteLine("-----------------------------");
                        WriteLine($"Result #{result.Key}");
                        var searchResult = result.Value;
                        WriteLine($"- Class: {searchResult.GetType()}");
                        WriteLine($"- Title: {searchResult.Title}");
                        WriteLine($"- Description: {searchResult.Description}");
                        WriteLine($"- URL: {searchResult.Url}");

                        var snippets = searchResult.RichSnippets;
                        if (snippets.Count() > 0)
                        {
                            WriteLine($"- Rich snippets: ");
                            foreach (var snippet in snippets)
                            {
                                WriteLine($"  * Snippet of type {snippet.GetType()} with data: {JsonConvert.SerializeObject(snippet)}");
                            }
                        }

                        WriteLine("\n");
                    }
                }
            }
        }

        private static void DumpSerpsSummary(SerpsSummaryEntry summary)
        {
            if (summary.Location != null && summary.Location.Length > 0)
            {
                WriteLine($"  Location: {summary.Location}");
            }

            foreach (var record in summary)
            {
                WriteLine($"  {record.Key}: {record.Value}");
            }
        }
    }
}
