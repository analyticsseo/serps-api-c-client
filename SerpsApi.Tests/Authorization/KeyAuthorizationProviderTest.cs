using System;
using Xunit;
using Authoritas.SerpsApi.Authorization;

namespace Authoritas.SerpsApi.Tests.Authorization
{
    public class KeyAuthorizationProviderTest
    {
        [Fact]
        public void AuthTokenGenerationTest()
        {
            var publicKey = "my public key";
            var privateKey = "my private key";
            var salt = "some salt";
            var auth = new KeyAuthorizationProvider(publicKey, privateKey, salt);
            var moment = new DateTimeOffset(2020, 1, 1, 12, 0, 0, new TimeSpan(-5, 0, 0));
            var token = auth.GetAuthorizationToken(moment);
            Assert.Equal("KeyAuth publicKey=my public key hash=69c976dbdc54be5a0e4b030e68a8f882adb52c061813067c86fa9828ab088334 ts=1577898000", token);
        }
    }
}
