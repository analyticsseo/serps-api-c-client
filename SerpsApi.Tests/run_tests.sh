#!/bin/bash

RESULT_DIR=Coverage
RESULT_FILE=coverage.cobertura.xml

dotnet test --collect:"XPlat Code Coverage" --results-directory:./$RESULT_DIR

OUT_DIR=$(ls -atd $RESULT_DIR/*/ | tail -n 1)
mv -f $OUT_DIR/$RESULT_FILE $RESULT_DIR
rmdir $OUT_DIR

~/.dotnet/tools/reportgenerator "-reports:./$RESULT_DIR/$RESULT_FILE" "-targetdir:./$RESULT_DIR" -reporttypes:Html
