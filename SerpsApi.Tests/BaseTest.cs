using System;
using System.IO;
using System.Reflection;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;

namespace Authoritas.SerpsApi.Tests
{
    public class BaseTest
    {
        protected string GetFilePath(string relativePath)
        {
            var basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return Path.GetFullPath(Path.Combine(basePath, "../../../Data", relativePath));
        }

        protected string GetFileContent(string relativePath)
        {
            var path = GetFilePath(relativePath);
            return File.ReadAllText(path);
        }

        public HttpClient MockHttpClient(string expectedResponse)
        {
            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(expectedResponse),
            };

            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(response, TimeSpan.FromMilliseconds(10));

            var client = new HttpClient(handlerMock.Object);
            client.BaseAddress = new Uri("http://localhost/");

            return client;
        }
    }
}
