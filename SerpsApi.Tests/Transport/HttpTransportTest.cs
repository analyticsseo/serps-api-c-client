using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Xunit;
using Authoritas.SerpsApi.Authorization;
using Authoritas.SerpsApi.Transport;

namespace Authoritas.SerpsApi.Tests.Transport
{
    public class HttpTransportTest : BaseTest
    {
        [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
        class DummyObject
        {
            public string Value { get; set; }
        }

        [Fact]
        public async Task ExecuteQueryTest()
        {
            var expectedValue = "Expected string value";
            var expectedResponse = $"{{ \"value\": \"{expectedValue}\" }}";
            var httpClient = MockHttpClient(expectedResponse);
            var auth = new KeyAuthorizationProvider(string.Empty, string.Empty, string.Empty);

            var httpTransport = new HttpTransport(httpClient, auth);
            var response = await httpTransport.ExecuteQuery<DummyObject>("endpoint");

            Assert.NotNull(response);
            Assert.IsType<DummyObject>(response);
            Assert.Equal(expectedValue, response.Value);
        }

        [Fact]
        public async Task ExecuteCommandTest()
        {
            var request = new DummyObject
            {
                Value = "Request object content"
            };

            var expectedValue = "Response object content";
            var expectedResponse = $"{{ \"value\": \"{expectedValue}\" }}";

            var httpClient = MockHttpClient(expectedResponse);
            var auth = new KeyAuthorizationProvider(string.Empty, string.Empty, string.Empty);

            var httpTransport = new HttpTransport(httpClient, auth);
            var response = await httpTransport.ExecuteCommand<DummyObject, DummyObject>("endpoint", request);

            Assert.NotNull(response);
            Assert.IsType<DummyObject>(response);
            Assert.Equal(expectedValue, response.Value);
            Console.WriteLine(expectedValue);
        }
    }
}
