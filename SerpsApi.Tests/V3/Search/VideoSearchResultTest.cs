using System;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class VideoSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/VideoSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as VideoSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(VideoSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Django Unchained - Movie Review by Chris Stuckmann", result.Title);
            Assert.Equal("Chris Stuckmann", result.Description);
            Assert.Equal(new Uri("https://www.youtube.com/watch?v=ox6ZwZ4xdzk"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Equal(3, result.Markup);
            Assert.Equal("180,1740", result.TopLeft);
            Assert.Equal("832,2325", result.BottomRight);
            Assert.True(result.Visible);
            Assert.False(result.AboveTheFold);
            Assert.Empty(result.RichSnippets);

            // Specific fields
            Assert.False(result.Featured);
        }
    }
}
