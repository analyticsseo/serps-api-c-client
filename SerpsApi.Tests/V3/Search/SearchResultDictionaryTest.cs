using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class SearchResultDictionaryTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var json = GetFileContent("SearchResultDictionary.json");
            var results = JsonConvert.DeserializeObject<SearchResultDictionary>(json);
            Assert.NotNull(results);
            Assert.NotEmpty(results);
            var output = JsonConvert.SerializeObject(results, Formatting.Indented) + "\n";
            Assert.Equal(json, output);
        }
    }
}
