using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class PersonRichSnippetTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var json = GetFileContent("RichSnippet/PersonRichSnippet.json");
            var snippet = JsonConvert.DeserializeObject<RichSnippet>(json);

            Assert.NotNull(snippet);
            Assert.IsType<PersonRichSnippet>(snippet);

            var rating = snippet as PersonRichSnippet;
            Assert.Equal(PersonRichSnippet.DiscriminatorValue, rating.Type);
            Assert.Equal("Twickenham, Greater London, United Kingdom", rating.Location);
            Assert.Equal("‎CEO at Analytics SEO", rating.Position);
        }
    }
}
