using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class ResearchGuideSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/ResearchGuideSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as ResearchGuideSearchResult;
            Assert.NotNull(result);
            Assert.Equal(ResearchGuideSearchResult.DiscriminatorValue, result.Type);
            Assert.False(result.Amp);
            Assert.False(result.Carousel);
        }
    }
}
