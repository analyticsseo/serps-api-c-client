using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class SiteLinksRichSnippetTests : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var json = GetFileContent("RichSnippet/SiteLinksRichSnippet.json");
            var snippet = JsonConvert.DeserializeObject<RichSnippet>(json);

            Assert.NotNull(snippet);
            Assert.IsType<SiteLinksRichSnippet>(snippet);

            var links = snippet as SiteLinksRichSnippet;
            Assert.Collection(
                links.Links,
                link =>
                {
                    Assert.Equal(new Uri("http://www.anandtech.com/show/8526/nvidia-geforce-gtx-980-review/2"), link.Url);
                    Assert.Equal(string.Empty, link.Description);
                    Assert.Equal("Our deep dive on the Maxwell ...", link.Title);
                },
                link =>
                {
                    Assert.Equal(new Uri("http://www.anandtech.com/show/8526/nvidia-geforce-gtx-980-review/22"), link.Url);
                    Assert.Equal(string.Empty, link.Description);
                    Assert.Equal("Overclocking GTX 980", link.Title);
                },
                link =>
                {
                    Assert.Equal(new Uri("http://www.anandtech.com/show/8526/nvidia-geforce-gtx-980-review/20"), link.Url);
                    Assert.Equal(string.Empty, link.Description);
                    Assert.Equal("Compute", link.Title);
                },
                link =>
                {
                    Assert.Equal(new Uri("http://www.anandtech.com/show/8526/nvidia-geforce-gtx-980-review/13"), link.Url);
                    Assert.Equal(string.Empty, link.Description);
                    Assert.Equal("Battlefield 4", link.Title);
                }
            );
        }

        [Fact]
        public void EmptyLinksTest()
        {
            var snippet = JObject.Parse("{ \"type\": \"site_links\" }").ToObject<RichSnippet>();
            Assert.NotNull(snippet);
            Assert.IsType<SiteLinksRichSnippet>(snippet);
            Assert.Empty((snippet as SiteLinksRichSnippet).Links);
        }
    }
}
