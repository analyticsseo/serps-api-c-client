using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class PeopleAlsoSearchForSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/PeopleAlsoSearchForSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as PeopleAlsoSearchForSearchResult;
            Assert.NotNull(result);
            Assert.Equal(PeopleAlsoSearchForSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal(new Dictionary<string, string>()
            {
                { "1", "Grimes" },
                { "2", "Jeff Bezos" },
                { "3", "Talulah Riley" },
                { "4", "Justine Musk" },
                { "5", "Mark Zuckerberg" },
            }, result.Phrases);
        }
    }
}
