using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class ImageSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/ImageSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as ImageSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(ImageSearchResult.DiscriminatorValue, result.Type);
            Assert.Null(result.Title);
            Assert.Null(result.Description);
            Assert.Equal(new Uri("https://shop.ariustechnology.com/blogs/news/top-5-most-expensive-claude-monet-paintings-ever-sold"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Empty(result.RichSnippets);
            Assert.Equal(1, result.Markup);
            Assert.Equal("180,1352", result.TopLeft);
            Assert.Equal("356,1482", result.BottomRight);
            Assert.True(result.Visible);
            Assert.False(result.AboveTheFold);

            // Specific fields
            Assert.Equal("left", result.Position);
        }
    }
}
