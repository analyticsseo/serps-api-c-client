using System;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class ShoppingItemSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/ShoppingItemSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as ShoppingItemSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(ShoppingItemSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Детектор углекислого газа", result.Title);
            Assert.Equal("3 990 ₽", result.Description);
            Assert.Equal(new Uri("https://pokupki.market.yandex.ru/product/detektor-uglekislogo-gaza/100892589243?utm_term=16225753%7C100892589243"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Equal(1, result.Markup);
            Assert.Equal("313,206", result.TopLeft);
            Assert.Equal("435,522", result.BottomRight);
            Assert.True(result.Visible);
            Assert.True(result.AboveTheFold);
            Assert.Empty(result.RichSnippets);

            // Specific fields
            Assert.Equal("Яндекс.Маркет", result.Merchant);
            Assert.Equal(3.8M, result.Rating);
            Assert.Equal("left", result.Position);
            Assert.Null(result.Info);
            Assert.False(result.ShopFor);
        }
    }
}
