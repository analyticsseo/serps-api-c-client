using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class NewsSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/NewsSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as NewsSearchResult;
            Assert.NotNull(result);

            Assert.Equal(NewsSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Elon Musk Says There's A Reason Why Only 2 US Carmakers Have Avoided Bankruptcy Out Of Thousands", result.Title);
            Assert.False(result.Amp);
            Assert.True(result.Carousel);
            Assert.False(result.Video);
        }
    }
}
