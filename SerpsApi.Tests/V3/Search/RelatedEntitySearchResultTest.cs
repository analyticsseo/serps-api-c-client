using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class RelatedEntitySearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/RelatedEntitySearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as RelatedEntitySearchResult;
            Assert.NotNull(result);
            Assert.Equal(RelatedEntitySearchResult.DiscriminatorValue, result.Type);
            Assert.Equal(new Dictionary<string, string>()
            {
                { "1", "Item 1" },
                { "2", "Item 2" },
                { "3", "Item 3" },
                { "4", "Item 4" },
                { "5", "Item 5" },
            }, result.Items);
        }
    }
}
