using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class PodcastSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/PodcastSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as PodcastSearchResult;
            Assert.NotNull(result);
            Assert.Equal(PodcastSearchResult.DiscriminatorValue, result.Type);
            Assert.True(result.Carousel);
            Assert.Equal("2 weeks ago", result.Age);
            Assert.Equal("12m", result.Length);
        }
    }
}
