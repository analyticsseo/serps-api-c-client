using System;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class AdvertisementSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/AdvertisementSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as AdvertisementSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(AdvertisementSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("IT Careers at USAA - Hiring IT Professionals - usaajobs.com", result.Title);
            Assert.Equal("Take on IT work that really matters. See what we're made of. Apply today. "
                + "Serving members with nearly 100 years of innnovation. "
                + "Learn more. Browse Locations. Sign Up For Job Alerts. Search Jobs. "
                + "Highlights: In Business Since 1922, Providing Job Alerts.", result.Description);
            Assert.Equal(new Uri("https://www.usaajobs.com/it-careers"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Empty(result.RichSnippets);
            Assert.Equal(1, result.Markup);
            Assert.Equal("180,187", result.TopLeft);
            Assert.Equal("780,401", result.BottomRight);
            Assert.True(result.Visible);
            Assert.True(result.AboveTheFold);

            // Specific fields
            Assert.True(result.OnTop);
        }
    }
}
