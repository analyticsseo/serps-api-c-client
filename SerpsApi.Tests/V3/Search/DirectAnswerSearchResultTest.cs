using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class DirectAnswerSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/DirectAnswerSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as DirectAnswerSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(DirectAnswerSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Nutmeg: Health Benefits, Nutrition Information, and How to Use It", result.Title);
            Assert.Null(result.Description);
            Assert.Equal(new Uri("https://www.webmd.com/diet/nutmeg-health-benefits-nutrition-uses"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Empty(result.RichSnippets);
            Assert.Equal(1, result.Markup);
            Assert.Equal("180,529", result.TopLeft);
            Assert.Equal("832,903", result.BottomRight);
            Assert.True(result.Visible);
            Assert.True(result.AboveTheFold);

            // Specific fields
            Assert.Equal("list", result.SubType);
            Assert.Equal("Nutmeg is rich in fiber, which helps keep the digestive system "
                + "healthy and prevent blood sugar from spiking. It's also a source of: "
                + "Vitamin A. Vitamin C....A 1 tsp serving of nutmeg contains:", result.Header);

            Assert.Equal(new Dictionary<string, string>()
            {
                { "1", "Calories: 12." },
                { "2", "Protein: 0 grams." },
                { "3", "Fat: 1 gram." },
                { "4", "Carbohydrates: 1 gram." },
                { "5", "Fiber: 0 grams." },
                { "6", "Sugar: 1 gram." },
            }, result.Steps);
        }
    }
}
