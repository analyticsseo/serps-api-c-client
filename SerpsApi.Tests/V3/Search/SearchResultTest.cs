using System;
using Newtonsoft.Json.Linq;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class SearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var rawObject = JObject.Parse("{ \"field\": \"value\", \"sub\": { \"1\": 1, \"2\": 2 } }");
            var search = rawObject.ToObject<SearchResult>();

            // Empty snippet collections
            Assert.Empty(search.RichSnippets);

            // Property accessors
            Assert.Equal("value", (string) search["field"]);
            Assert.Equal("value", search["field"].Value<string>());
            Assert.Equal("value", search.GetProperty<string>("field"));

            // Non-existing property access
            Assert.Null(search.GetProperty<string>("not_exists"));
            Assert.Null((string) search["not_exists"]);
            Assert.Throws<ArgumentNullException>(() => Assert.Null(search["not_exists"].Value<string>()));

            // Nested property access
            Assert.Equal(1, search["sub"]["1"].Value<int>());
            Assert.Equal(2, (int) search["sub"]["2"]);

            // Invalid key type
            Assert.Throws<ArgumentException>(() => search["sub"][2].Value<int>());
        }
    }
}
