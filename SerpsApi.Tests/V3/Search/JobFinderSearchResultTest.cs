using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class JobFinderSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/JobFinderSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as JobFinderSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(JobFinderSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Jobs", result.Title);
            Assert.Equal("Near United States", result.Description);
            Assert.Null(result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Empty(result.RichSnippets);
            Assert.Equal(2, result.Markup);
            Assert.Equal("181,592", result.TopLeft);
            Assert.Equal("831,1134", result.BottomRight);
            Assert.True(result.Visible);
            Assert.True(result.AboveTheFold);

            // Specific fields
            Assert.Equal(new Dictionary<string, string>() {
                { "1", "Full Stack Software Developer" },
                { "2", "Senior Software Development Engineer- Remote" },
                { "3", "Software Developer" },
            }, result.Items);

            Assert.Equal(new Dictionary<string, string>() {
                { "1", "Privacy settings" },
                { "2", "How Search works" },
                { "3", "Past 3 days" },
                { "4", "Full-time" },
                { "5", "Work from home" },
                { "6", "Software engineer" },
                { "7", "Development engineer" },
                { "8", "Senior" },
                { "9", "Development manager" },
                { "10", "Engineer" },
                { "11", "Principal" },
                { "12", "Engineering" },
            }, result.Filters);
        }
    }
}
