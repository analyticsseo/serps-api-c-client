using System;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class PeopleAlsoAskSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/PeopleAlsoAskSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as PeopleAlsoAskSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(PeopleAlsoAskSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Software Developer Careers | ComputerScience.org", result.Title);
            Assert.Null(result.Description);
            Assert.Equal(new Uri("https://www.computerscience.org/careers/software-developer/"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Empty(result.RichSnippets);
            Assert.Equal(1, result.Markup);
            Assert.Equal("180,1293", result.TopLeft);
            Assert.Equal("832,1328", result.BottomRight);
            Assert.True(result.Visible);
            Assert.False(result.AboveTheFold);

            // Specific fields
            Assert.Equal("What careers are in software development?", result.Question);
            Assert.Equal("Web Developer. Web developers design and build web pages. ...  "
                + "Computer Programmer. Computer programmers build computer applications "
                + "by writing code in various computer languages. ...  "
                + "Database Administrator. ...  Software Developer. ", result.Answer);
        }
    }
}
