using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class RefineBySearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/RefineBySearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as RefineBySearchResult;
            Assert.NotNull(result);
            Assert.Equal(RefineBySearchResult.DiscriminatorValue, result.Type);
            Assert.False(result.Brand);
            Assert.Equal(new Dictionary<string, string>()
            {
                { "1", "Nike Air" },
                { "2", "Nike Flyknit" },
                { "3", "Nike LeBron" },
                { "4", "Nike Blazer" },
                { "5", "Nike Zoom" },
                { "6", "Nike Kyrie" },
                { "7", "Nike Metcon" },
                { "8", "Nike Pegasus" },
                { "9", "Nike Renew" },
                { "10", "Nike Revolution" },
                { "11", "Nike Downshifter" },
                { "12", "Paul George Collection" },
            }, result.Items);
        }
    }
}
