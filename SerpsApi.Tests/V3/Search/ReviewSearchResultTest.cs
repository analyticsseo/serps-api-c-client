using System;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class ReviewSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/ReviewSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as ReviewSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(ReviewSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Django Unchained, Reviews", result.Title);
            Assert.Equal("2012 film", result.Description);
            Assert.Null(result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Equal(1, result.Markup);
            Assert.Equal("180,172", result.TopLeft);
            Assert.Equal("832,288", result.BottomRight);
            Assert.True(result.Visible);
            Assert.True(result.AboveTheFold);
            Assert.Empty(result.RichSnippets);

            // Specific fields
            Assert.Collection(
                result.Items.Values,
                item =>
                {
                    Assert.Equal(new Uri("https://www.imdb.com/title/tt0001853728/"), item.Url);
                    Assert.Equal("IMDb", item.Name);
                    Assert.Equal("8.4/10", item.Review);
                    Assert.True(item.Visible);
                },
                item =>
                {
                    Assert.Equal(new Uri("https://www.rottentomatoes.com/m/django_unchained_2012"), item.Url);
                    Assert.Equal("Rotten Tomatoes", item.Name);
                    Assert.Equal("87%", item.Review);
                    Assert.True(item.Visible);
                },
                item =>
                {
                    Assert.Equal(new Uri("https://www.metacritic.com/movie/django-unchained"), item.Url);
                    Assert.Equal("Metacritic", item.Name);
                    Assert.Equal("81%", item.Review);
                    Assert.True(item.Visible);
                }
            );
        }
    }
}
