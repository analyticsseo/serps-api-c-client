using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class DiscoverMorePlacesSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/DiscoverMorePlacesSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as DiscoverMorePlacesSearchResult;
            Assert.NotNull(result);

            Assert.Equal(DiscoverMorePlacesSearchResult.DiscriminatorValue, result.Type);
            Assert.True(result.Carousel);
        }
    }
}
