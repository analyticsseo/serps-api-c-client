using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class SocialMediaSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/SocialMediaSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as SocialMediaSearchResult;
            Assert.NotNull(result);

            Assert.Equal(SocialMediaSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("tweet", result.SubType);
            Assert.True(result.Carousel);
            Assert.False(result.Video);
        }
    }
}
