using System;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class OrganicSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/OrganicSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as OrganicSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(OrganicSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Software Developer Jobs, Employment | Indeed.com", result.Title);
            Assert.Equal("Remote. All jobs. Last 24 hours. Last 3 days. Last 7 days. Salary. "
                + "All job categories. Technology. Architecture & Engineering. All skills. "
                + "JavaScript. Java. All education levels. Bachelor's degree. Master's degree. "
                + "All clearance types. Secret Clearance. TS/SCI. All jobs. "
                + "Flexible schedule. Monday to Friday.", result.Description);
            Assert.Equal(new Uri("https://www.indeed.com/q-Software-Developer-jobs.html"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Equal(1, result.Markup);
            Assert.Equal("180,1525", result.TopLeft);
            Assert.Equal("780,1668", result.BottomRight);
            Assert.True(result.Visible);
            Assert.False(result.AboveTheFold);

            // Snippets
            Assert.Collection(result.RichSnippets, snippet =>
            {
                var bc = snippet as BreadcrumbRichSnippet;
                Assert.NotNull(bc);
                Assert.Equal(BreadcrumbRichSnippet.DiscriminatorValue, bc.Type);
                Assert.Equal("www.indeed.com › q-Software-Developer-jobs", bc.Path);
            });

            // Specific fields
            Assert.False(result.Amp);
            Assert.False(result.Carousel);
            Assert.Null(result.Faq);
            Assert.Null(result.Thumbnail);
            Assert.Null(result.VideoThumbnail);
        }
    }
}
