using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class KnowledgeGraphSearchResultTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var sample = GetFileContent("SearchResult/KnowledgeGraphSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample) as KnowledgeGraphSearchResult;
            Assert.NotNull(result);

            // Common search result fields
            Assert.Equal(KnowledgeGraphSearchResult.DiscriminatorValue, result.Type);
            Assert.Equal("Leaning Tower of Pisa", result.Title);
            Assert.Equal("The Leaning Tower of Pisa or simply the Tower of Pisa is the campanile, "
                + "or freestanding bell tower, of the cathedral of the Italian city of Pisa, "
                + "known worldwide for its nearly four-degree lean, "
                + "the result of an unstable foundation.", result.Description);
            Assert.Equal(new Uri("https://en.wikipedia.org/wiki/Leaning_Tower_of_Pisa"), result.Url);
            Assert.Equal(1, result.PageNumber);
            Assert.Empty(result.RichSnippets);
            Assert.Equal(1, result.Markup);
            Assert.Equal("893,248", result.TopLeft);
            Assert.Equal("1349,2604", result.BottomRight);
            Assert.True(result.Visible);
            Assert.False(result.AboveTheFold);

            // Specific fields
            Assert.Equal("https://en.wikipedia.org/wiki/Leaning_Tower_of_Pisa", result.EntityUrl);
            Assert.Equal("right", result.Position);
            Assert.Equal("local_business_listing", result.SubType);
            Assert.Equal("Tower in Pisa, Italy", result.SubTitle);

            Assert.Equal(new Dictionary<string, Uri>() {
                { "facebook", new Uri("https://www.facebook.com/leanintowerpisa/") }
            }, result.Profiles);

            Assert.Collection(
                result.OtherItems.Values,
                item =>
                {
                    Assert.Equal("hw:/collection/locations:materials", item.Category);
                    Assert.Equal("Materials", item.Label);
                    Assert.Equal("Marble, Rock", item.Value);
                },
                item =>
                {
                    Assert.Equal("kc:/local:located in", item.Category);
                    Assert.Equal("Located in", item.Label);
                    Assert.Equal("Piazza del Duomo", item.Value);
                },
                item =>
                {
                    Assert.Equal("kc:/architecture/structure:construction started", item.Category);
                    Assert.Equal("Construction started", item.Label);
                    Assert.Equal("August 9, 1173", item.Value);
                }
            );

            // Single item access
            var item1 = result["other_items"]["1"].ToObject<KnowledgeGraphSearchResult.Item>();
            Assert.NotNull(item1);
            Assert.Equal("hw:/collection/locations:materials", item1.Category);
            Assert.Equal("Materials", item1.Label);
            Assert.Equal("Marble, Rock", item1.Value);
        }
    }
}
