using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.V3.Search
{
    public class RatingRichSnippetTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var json = GetFileContent("RichSnippet/RatingRichSnippet.json");
            var snippet = JsonConvert.DeserializeObject<RichSnippet>(json);

            Assert.NotNull(snippet);
            Assert.IsType<RatingRichSnippet>(snippet);

            var rating = snippet as RatingRichSnippet;
            Assert.Equal(RatingRichSnippet.DiscriminatorValue, rating.Type);
            Assert.Equal("Rating: 8.4/10 · 1,367,827 votes", rating.Raw);
            Assert.Equal(8.4M, rating.Score);
            Assert.Equal("review", rating.GuessedType);
            Assert.Equal(1367827, rating.Votes);
            Assert.Null(rating.Reviewer);
            Assert.Null(rating.Date);
            Assert.Null(rating.Time);
            Assert.Null(rating.Price);
        }
    }
}
