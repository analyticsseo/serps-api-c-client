using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3;

namespace Authoritas.SerpsApi.Tests.V3
{
    public class SerpsSummaryEntryTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var json = GetFileContent("SerpsSummaryEntry.json");
            var summary = JsonConvert.DeserializeObject<SerpsSummaryEntry>(json);
            Assert.NotNull(summary);
            Assert.NotEmpty(summary);

            Assert.Equal("United States", summary.Location);
            Assert.Equal(4, summary["ad"]);
            Assert.Equal(0, summary["destination"]);
            Assert.Equal(1510000000, summary["total"]);
            Assert.Equal(0, summary["missing_field"]);

            var output = JsonConvert.SerializeObject(summary, Formatting.Indented) + "\n";
            Assert.Equal(json, output);
        }

        [Fact]
        public void InvalidSourceJsonTest()
        {
            Assert.Throws<JsonException>(() =>
            {
                JsonConvert.DeserializeObject<SerpsSummaryEntry>("[]");
            });
        }
    }
}
