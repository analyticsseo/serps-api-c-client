using System.Collections.Generic;
using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3;

namespace Authoritas.SerpsApi.Tests.V3
{
    public class SerpsSummaryTest : BaseTest
    {
        [Fact]
        public void SerializationTest()
        {
            var json = GetFileContent("SerpsSummary.json");
            var summary = JsonConvert.DeserializeObject<SerpsSummary>(json);
            Assert.NotNull(summary);
            Assert.NotNull(summary.Global);
            Assert.Equal("United States", summary.Global.Location);
            Assert.Equal(4, summary.Global["ad"]);
            Assert.Equal(2, summary.Global["organic"]);
            Assert.Equal(100, summary.Global["total"]);
            Assert.NotNull(summary.Pages);
            Assert.Collection(
                summary.Pages.Keys,
                k => Assert.Equal("1", k),
                k => Assert.Equal("2", k)
            );
            Assert.Collection(
                summary.Pages.Values,
                value =>
                {
                    Assert.Equal("United States", value.Location);
                    Assert.Equal(4, value["ad"]);
                    Assert.Equal(2, value["organic"]);
                },
                value =>
                {
                    Assert.Equal("United States", value.Location);
                    Assert.Equal(4, value["ad"]);
                    Assert.Equal(2, value["organic"]);
                }
            );
        }
    }
}
