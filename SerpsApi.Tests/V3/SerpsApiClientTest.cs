using System;
using Xunit;
using Authoritas.SerpsApi.Authorization;
using Authoritas.SerpsApi.Transport;
using Authoritas.SerpsApi.V3;

namespace Authoritas.SerpsApi.Tests.V3
{
    public class SerpsApiClientTest : BaseTest
    {
        [Fact]
        public async void CreateJobTest()
        {
            var jobId = "1ce03d9c-89b5-4fb6-8619-b74312917155";
            var expectedResponse = GetFileContent("CreateJob.json");
            var httpClient = MockHttpClient(expectedResponse);
            var auth = new KeyAuthorizationProvider(string.Empty, string.Empty, string.Empty);
            var transport = new HttpTransport(httpClient, auth);
            var client = new SerpsApiClient(transport);
            var response = await client.CreateJob(new SerpsRequest());

            Assert.Equal(jobId, response.JobId);
            Assert.False(response.Ready);
            Assert.Null(response.Error);
            Assert.NotNull(response.Request);
            Assert.Equal(jobId, response.Request.JobId);
            Assert.Equal(SearchEngine.Google, response.Request.SearchEngine);
            Assert.Equal("Software development jobs", response.Request.Phrase);
            Assert.Equal("us", response.Request.Region);
            Assert.Equal("en", response.Request.Language);
            Assert.Equal(UserAgent.PC, response.Request.UserAgent);
            Assert.Equal("web", response.Request.SearchType);
            Assert.Equal(100, response.Request.MaxResults);
            Assert.Equal(new DateTime(2021, 3, 5, 9, 44, 47), response.Request.CreatedDate);
            Assert.Null(response.Request.ProcessedDate);
            Assert.Equal(new Uri("http://localhost:8080"), response.Request.Callback);
            Assert.Equal("56.326790", response.Request.Latitude);
            Assert.Equal("44.005989", response.Request.Longitude);
            Assert.Equal("Some town", response.Request.Town);
            Assert.True(response.Request.UseCache);
            Assert.False(response.Request.CacheHit);
            Assert.False(response.Request.ErrorFlag);
            Assert.False(response.Request.IncludeAllInUniversal);
        }

        [Fact]
        public async void GetJobResultTest()
        {
            var jobId = "1ce03d9c-89b5-4fb6-8619-b74312917155";
            var expectedResponse = GetFileContent("GetJobResult.json");
            var httpClient = MockHttpClient(expectedResponse);
            var auth = new KeyAuthorizationProvider(string.Empty, string.Empty, string.Empty);
            var transport = new HttpTransport(httpClient, auth);
            var client = new SerpsApiClient(transport);
            var response = await client.GetJobResult(jobId);

            Assert.Equal(jobId, response.JobId);
            Assert.True(response.Ready);
            Assert.NotNull(response.Request);
            Assert.Equal(new DateTime(2021, 3, 5, 9, 47, 56), response.Request.ProcessedDate);
        }
    }
}
