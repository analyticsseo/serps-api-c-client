using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Tests.Serialization
{
    public class DynamicObjectTest : BaseTest
    {
        [Fact]
        public void GetPropertyTest()
        {
            var sample = GetFileContent("SearchResult/JobFinderSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample);
            Assert.NotNull(result);

            // Valid property access
            Assert.Equal(JobFinderSearchResult.DiscriminatorValue, result.GetProperty<string>("type"));

            // Invalid property type casting access
            Assert.Null(result.GetProperty<bool?>("type"));

            // Missing property access
            Assert.Null(result.GetProperty<bool?>("missing_prop"));
        }

        [Fact]
        public void GetDictionaryTest()
        {
            var sample = GetFileContent("SearchResult/JobFinderSearchResult.json");
            var result = JsonConvert.DeserializeObject<SearchResult>(sample);
            Assert.NotNull(result);

            // Valid dictionary access
            Assert.NotEmpty(result.GetDictionary<string, string>("items"));

            // Invalid typed dictionary
            Assert.Throws<JsonReaderException>(() => result.GetDictionary<string, bool>("items"));

            // Non-existing dictionary access
            Assert.Null(result.GetDictionary<string, string>("missing_property"));
        }
    }
}
