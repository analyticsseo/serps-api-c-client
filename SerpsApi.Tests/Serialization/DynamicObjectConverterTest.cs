using Newtonsoft.Json;
using Xunit;
using Authoritas.SerpsApi.V3.Search;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.Tests.Serialization
{
    using SnippetConverter = DynamicObjectConverter<RichSnippet, string>;

    public class DynamicObjectConverterTest : BaseTest
    {
        [Fact]
        public void CanConvertTest()
        {
            var converter = new SnippetConverter(string.Empty);
            Assert.True(converter.CanConvert(typeof(RichSnippet)));
            Assert.True(converter.CanConvert(typeof(BreadcrumbRichSnippet)));
            Assert.False(converter.CanConvert(typeof(ImageSearchResult)));
        }

        [Fact]
        public void InvalidSourceJsonTest()
        {
            var exception = Assert.Throws<JsonException>(() =>
            {
                JsonConvert.DeserializeObject<RichSnippet>("[]");
            });

            Assert.Equal(SnippetConverter.InvalidJsonExceptionMessage, exception.Message);
        }

        [Fact]
        public void MissingDiscriminatorFallbackTest()
        {
            var snippet = JsonConvert.DeserializeObject<RichSnippet>("{}");
            Assert.NotNull(snippet);
            Assert.IsType<RichSnippet>(snippet);
        }

        [Fact]
        public void InvalidDiscriminatorFallbackTest()
        {
            var snippet = JsonConvert.DeserializeObject<RichSnippet>("{ \"type\": [] }");
            Assert.NotNull(snippet);
            Assert.IsType<RichSnippet>(snippet);
        }

        [Fact]
        public void WriteJsonTest()
        {
            var json = GetFileContent("RichSnippet/BreadcrumbRichSnippet.json");
            var snippet = JsonConvert.DeserializeObject<RichSnippet>(json);
            Assert.NotNull(snippet);
            Assert.IsType<BreadcrumbRichSnippet>(snippet);
            var output = JsonConvert.SerializeObject(snippet);
            Assert.Equal("{\"type\":\"breadcrumb\",\"path\":\"www.indeed.com › q-Software-Developer-jobs\"}", output);
        }
    }
}
