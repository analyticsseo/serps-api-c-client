namespace Authoritas.SerpsApi.DependencyInjection
{
    /// <summary>
    /// Enumeration of supported SERPs API transport implementations.
    /// </summary>
    public enum SerpsApiTransport
    {
        Http,
    }
}
