using System;
using System.Net.Http;
using Authoritas.SerpsApi.Transport;
using Authoritas.SerpsApi.Authorization;

namespace Authoritas.SerpsApi.DependencyInjection
{
    /// <summary>
    /// SERPs API transport factory.
    /// </summary>
    public class SerpsApiTransportFactory
    {
        /// <summary>
        /// Creates transport of specified type with specified parameters.
        /// </summary>
        /// <param name="transport">SERPs API transport type.</param>
        /// <param name="apiUrl">SERPs API url.</param>
        /// <param name="authorizationProvider">SERPs API authorization provider.</param>
        /// <returns>SERPs API transport instance.</returns>
        public static ISerpsApiTransport CreateTransport(SerpsApiTransport transport, Uri apiUrl, KeyAuthorizationProvider authorizationProvider)
        {
            switch (transport)
            {
                case SerpsApiTransport.Http:
                    var httpClient = new HttpClient();
                    httpClient.BaseAddress = apiUrl;
                    return new HttpTransport(httpClient, authorizationProvider);

                default:
                    throw new NotImplementedException($"Unknown Serps API transport {transport}");
            }
        }
    }
}
