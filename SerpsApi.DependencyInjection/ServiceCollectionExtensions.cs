﻿using System;
using Authoritas.SerpsApi.Transport;
using Authoritas.SerpsApi.DependencyInjection;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the SERPs API client with specified parameters to the dependency injection container of the application.
        /// </summary>
        public static IServiceCollection AddSerpsApiClient(this IServiceCollection services, Action<SerpsApiOptions> setupAction)
        {
            // Invoking options configuration closure
            var options = new SerpsApiOptions();
            setupAction.Invoke(options);

            // API transport configuration
            var transport = SerpsApiTransportFactory.CreateTransport(options.Transport, options.ApiUrl, options.AuthorizationProvider);
            services.AddSingleton<ISerpsApiTransport>(transport);

            // Service registration based on requested API version
            switch (options.ApiVersion)
            {
                case SerpsApiVersion.V3:
                    services.AddSingleton<Authoritas.SerpsApi.V3.ISerpsApiClient, Authoritas.SerpsApi.V3.SerpsApiClient>();
                    break;
            }

            return services;
        }
    }
}
