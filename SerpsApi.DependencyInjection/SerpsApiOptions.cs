using System;
using Authoritas.SerpsApi.Authorization;

namespace Authoritas.SerpsApi.DependencyInjection
{
    /// <summary>
    /// SERPs API options builder
    /// </summary>
    public class SerpsApiOptions
    {
        /// <value>Default SERPs API URL, which will be used if none is provided from the configuration code.</value>
        public const string DefaultApiUrl = "http://v3.api.analyticsseo.com/";

        /// <value>Requested SERPs API version for the client.</value>
        public SerpsApiVersion ApiVersion { get; private set; } = SerpsApiVersion.V3;

        /// <value>SERPs API transport type.</value>
        public SerpsApiTransport Transport { get; set; } = SerpsApiTransport.Http;

        /// <value>SERPs API base url.</value>
        public Uri ApiUrl { get; set; } = new Uri(SerpsApiOptions.DefaultApiUrl);

        /// <value>SERPs API authorization provider.</value>
        public KeyAuthorizationProvider AuthorizationProvider { get; set; }

        /// <summary>
        /// Request specifid SERPs API version.
        /// If not called during the configuration stage, the default API version will be used.
        /// </summary>
        /// <param name="apiVersion">SERPs API version to use.</param>
        public void WithApiVersion(SerpsApiVersion apiVersion)
        {
            this.ApiVersion = apiVersion;
        }

        /// <summary>
        /// Request the SERPs API client to use HTTP transport with an optional api url provided.
        /// </summary>
        /// <param name="apiUrl">SERPs API URL to use. If not specified, the default will be used.</param>
        public void WithHttpTransport(Uri apiUrl = null)
        {
            Transport = SerpsApiTransport.Http;
            if (apiUrl != null)
            {
                ApiUrl = apiUrl;
            }
        }

        /// <summary>
        /// Request the SERPs API client to use key authorization with provided credentials.
        /// </summary>
        /// <param name="publicKey">Public key.</param>
        /// <param name="privateKey">Private key.</param>
        /// <param name="salt">Initial cryptographic salt.</param>
        public void WithKeyAuthorization(string publicKey, string privateKey, string salt)
        {
            AuthorizationProvider = new KeyAuthorizationProvider(publicKey, privateKey, salt);
        }
    }
}
