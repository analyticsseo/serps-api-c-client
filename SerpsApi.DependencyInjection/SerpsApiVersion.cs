namespace Authoritas.SerpsApi.DependencyInjection
{
    /// <summary>
    /// Enumeration of supported SERPs API verions.
    /// </summary>
    public enum SerpsApiVersion
    {
        V3 = 3,
    }
}
