using System.Threading.Tasks;

namespace Authoritas.SerpsApi.Transport
{
    /// <summary>
    /// Interface of SERPs API request transport.
    /// </summary>
    public interface ISerpsApiTransport
    {
        /// <summary>
        /// Serializes and sends the request object of specified type to the SERPs API, retrieves the response
        /// and deserializes it to an object of specified response type.
        /// </summary>
        /// <param name="endpoint">API endpoint (local to the base API URL).</param>
        /// <param name="request">Request object.</param>
        /// <returns>Deserialized response object.</returns>
        Task<ResponseObject> ExecuteCommand<RequestObject, ResponseObject>(string endpoint, RequestObject request);

        /// <summary>
        /// Queries a specified endpoint and returns the response as deserialized object of specified type.
        /// </summary>
        /// <param name="endpoint">API endpoint (local to the base API URL).</param>
        /// <returns>Deserialized response object.</returns>
        Task<ResponseObject> ExecuteQuery<ResponseObject>(string endpoint);
    }
}
