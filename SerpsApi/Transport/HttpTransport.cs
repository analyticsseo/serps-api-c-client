using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Authoritas.SerpsApi.Authorization;

namespace Authoritas.SerpsApi.Transport
{
    /// <summary>
    /// SERPs API HTTP transport implementation.
    /// </summary>
    public class HttpTransport : ISerpsApiTransport
    {
        private readonly HttpClient httpClient;

        private readonly KeyAuthorizationProvider authorizationProvider;

        /// <summary>
        /// Constructor for <c>HttpTransport.</c>.
        /// </summary>
        /// <param name="httpClient">Http client for underlying requests.</param>
        /// <param name="authorizationProvider">SERPs API key authorization provider.</param>
        public HttpTransport(HttpClient httpClient, KeyAuthorizationProvider authorizationProvider)
        {
            this.httpClient = httpClient;
            this.authorizationProvider = authorizationProvider;
        }

        /// <summary>
        /// Serializes and sends the request object of specified type to the SERPs API, retrieves the response
        /// and deserializes it to an object of specified response type.
        /// </summary>
        /// <param name="endpoint">API endpoint (local to the base API URL).</param>
        /// <param name="request">Request object.</param>
        /// <returns>Deserialized response object.</returns>
        public async Task<ResponseObject> ExecuteCommand<RequestObject, ResponseObject>(string endpoint, RequestObject request)
        {
            var message = BuildHttpRequestMessage(HttpMethod.Post, endpoint, request);
            return await ExecuteRequest<ResponseObject>(message);
        }

        /// <summary>
        /// Queries the specified endpoint and returns the response as deserialized object of specified type.
        /// </summary>
        /// <param name="endpoint">API endpoint (local to the base API URL).</param>
        /// <returns>Deserialized response object.</returns>
        public async Task<ResponseObject> ExecuteQuery<ResponseObject>(string endpoint)
        {
            var message = BuildHttpRequestMessage(HttpMethod.Get, endpoint);
            return await ExecuteRequest<ResponseObject>(message);
        }

        private HttpRequestMessage BuildHttpRequestMessage(HttpMethod method, string endpoint, object requestObject = null)
        {
            var message = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(endpoint, UriKind.Relative),
            };

            if (requestObject != null)
            {
                var serializedRequest = JsonConvert.SerializeObject(requestObject);
                message.Content = new StringContent(serializedRequest, Encoding.UTF8, "application/json");
            }

            if (authorizationProvider != null)
            {
                var token = authorizationProvider.GetAuthorizationToken(DateTimeOffset.UtcNow);
                message.Headers.Add(HttpRequestHeader.Authorization.ToString(), token);
            }

            return message;
        }

        private async Task<ResponseObject> ExecuteRequest<ResponseObject>(HttpRequestMessage message)
        {
            var response = await httpClient.SendAsync(message);
            using (Stream stream = await response.Content.ReadAsStreamAsync())
            using (StreamReader streamReader = new StreamReader(stream))
            using (JsonReader reader = new JsonTextReader(streamReader))
            {
                var serializer = JsonSerializer.Create(new JsonSerializerSettings
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc
                });

                return serializer.Deserialize<ResponseObject>(reader);
            }
        }
    }
}
