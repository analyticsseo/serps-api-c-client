using System;
using System.Text;
using System.Security.Cryptography;

namespace Authoritas.SerpsApi.Authorization
{
    /// <summary>
    /// Class <c>KeyAuthorizationProvider</c> provides SERPs API authorization token generation with specified key pair and a salt.
    /// </summary>
    public class KeyAuthorizationProvider
    {
        private readonly string publicKey = string.Empty;

        private readonly string privateKey = string.Empty;

        private readonly string salt = string.Empty;

        /// <summary>
        /// Constructor for <c>KeyAuthorizationProvider</c>.
        /// </summary>
        /// <param name="publicKey">Public key.</param>
        /// <param name="privateKey">Private key.</param>
        /// <param name="salt">Initial cryptographic salt.</param>
        public KeyAuthorizationProvider(string publicKey, string privateKey, string salt)
        {
            this.publicKey = publicKey ?? "";
            this.privateKey = privateKey ?? "";
            this.salt = salt ?? "";
        }

        /// <summary>
        /// Builds authorization token for SERPs API at given moment.
        /// </summary>
        /// <param name="moment">Moment of time.</param>
        /// <returns>Token to be used as the HTTP authorization header to access the SERPs API.</returns>
        public string GetAuthorizationToken(DateTimeOffset moment)
        {
            var time = moment.ToUnixTimeSeconds();
            var data = $"{time}{publicKey}{salt}";

            string hash = String.Empty;
            var key = Encoding.ASCII.GetBytes(privateKey);
            using (var hmac = new HMACSHA256(key))
            {
                var hashedData = hmac.ComputeHash(Encoding.ASCII.GetBytes(data));
                hash = BitConverter.ToString(hashedData).Replace("-", String.Empty).ToLower();
            }

            return $"KeyAuth publicKey={publicKey} hash={hash} ts={time}";
        }
    }
}
