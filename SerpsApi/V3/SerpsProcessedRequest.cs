using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// SERPs API processed request object is returned with the response.
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SerpsProcessedRequest : SerpsRequest
    {
        /// <value>The unique identifier for your job (job id).</value>
        [JsonProperty("jid")]
        public string JobId { get; set; }

        /// <value>Job created date.</value>
        public DateTime? CreatedDate { get; set; }

        /// <value>Job processed date.</value>
        public DateTime? ProcessedDate { get; set; }

        /// <value>True if the cache was hit.</value>
        public bool CacheHit { get; set; }

        /// <value>True if error happened during the request processing.</value>
        public bool ErrorFlag { get; set; }

        /// <value>Search type.</value>
        public string SearchType { get; set; }
    }
}
