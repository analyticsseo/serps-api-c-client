﻿using System;
using System.Threading.Tasks;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// Interface of the SERPs API client.
    /// </summary>
    public interface ISerpsApiClient
    {
        /// <summary>
        /// Create new SERPs processing request.
        /// </summary>
        /// <param name="request">SERPs API request object.</param>
        Task<SerpsResponse> CreateJob(SerpsRequest request);

        /// <summary>
        /// Retrieve SERPs processing request by job identifier.
        /// </summary>
        /// <param name="jobId">SERPs API job identifier.</param>
        Task<SerpsResponse> GetJobResult(string jobId);
    }
}
