using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// SERPs API response result data.
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SerpsResponseData
    {
        /// <value>Contains structured information about the search results entries found grouped by type.</value>
        public Dictionary<string, SearchResultDictionary> Results { get; set; }

        /// <value>The summary object of the SERPs API response data.</value>
        public SerpsSummary Summary { get; set; }
    }
}
