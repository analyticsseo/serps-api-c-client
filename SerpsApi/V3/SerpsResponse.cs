using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// SERPs API response object.
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SerpsResponse
    {
        /// <value>The unique identifier for your job (job id).</value>
        [JsonProperty("jid")]
        public string JobId { get; set; }

        /// <value>If <c>false</c> then the job is still being processed.</value>
        public bool Ready { get; set; }

        /// <value>Contains a copy of your request.</value>
        public SerpsProcessedRequest Request { get; set; }

        /// <value>Contains an error message, only present if there is an error.</value>
        public string Error { get; set; }

        /// <value>When ready it will contain the processed SERPs page results.</value>
        public SerpsResponseData Response { get; set; }
    }
}
