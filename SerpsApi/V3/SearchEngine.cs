namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// Container of search engine values supported by the SERPs API.
    /// </summary>
    public static class SearchEngine
    {
        public const string Bing = "bing";
        public const string Google = "google";
        public const string Yahoo = "yahoo";
        public const string Yandex = "yandex";
        public const string Baidu = "baidu";
    }
}
