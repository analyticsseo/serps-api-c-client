﻿using System;
using System.Threading.Tasks;
using Authoritas.SerpsApi.Transport;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// Client implementation for the SERPs API V3.
    /// </summary>
    public class SerpsApiClient : ISerpsApiClient
    {
        /// <value>
        /// SERPs API endpoint.
        /// </value>
        public const string EndpointSerps = "serps/";

        private readonly ISerpsApiTransport transport;

        /// <summary>
        /// Constructor for <c>SerpsApiClient</c>.
        /// </summary>
        /// <param name="transport">SERPs API client transport.</param>
        public SerpsApiClient(ISerpsApiTransport transport)
        {
            this.transport = transport;
        }

        /// <summary>
        /// Create new SERPs processing request.
        /// </summary>
        public async Task<SerpsResponse> CreateJob(SerpsRequest request)
        {
            return await transport.ExecuteCommand<SerpsRequest, SerpsResponse>(SerpsApiClient.EndpointSerps, request);
        }

        /// <summary>
        /// Retrieve SERPs processing result by job identifier.
        /// </summary>
        public async Task<SerpsResponse> GetJobResult(string jobId)
        {
            var endpoint = $"{SerpsApiClient.EndpointSerps}{jobId}";
            return await transport.ExecuteQuery<SerpsResponse>(endpoint);
        }
    }
}
