using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// The summary object of the Serps API response data.
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SerpsSummary
    {
        /// <value>
        /// Shows an aggregated count of the different search results found
        /// and the total number of results reported by the search engine.
        /// </value>
        public SerpsSummaryEntry Global { get; set; }

        /// <value>
        /// Contains a breakdown of the search results per page.
        /// The key of the dictionary is the page number.
        /// </value>
        /// <example>
        /// This sample shows how to enumerate all of the pages and output the summary for each of them:
        /// <code>
        /// foreach (var page in response.Response.Summary.Pages)
        /// {
        ///     var pageNumber = page.Key;
        ///     var pageSummary = page.Value;
        ///     Console.WriteLine($"Page {pageNumber} results:");
        ///     foreach (var entry in pageSummary)
        ///     {
        ///         Console.WriteLine($"  {entry.Key}: {entry.Value}");
        ///     }
        /// }
        /// </code>
        /// </example>
        public Dictionary<string, SerpsSummaryEntry> Pages { get; set; }
    }
}
