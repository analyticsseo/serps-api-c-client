namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// Container of user agent values supported by the SERPs API.
    /// </summary>
    public static class UserAgent
    {
        /// <value>The default user agent, emulates Chrome browser on Windows.</value>
        public const string PC = "pc";

        /// <value>Chrome browser on Android (mobile).</value>
        public const string Mobile = "mobile";
    }
}
