using System.Collections.Generic;
using Newtonsoft.Json;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// Dictionary container for SERPs summary entry.
    /// </summary>
    /// <example>
    /// This sample shows how to enumerate all of the available values in the summary entry:
    /// <code>
    /// foreach (var entry in response.Response.Summary.Global)
    /// {
    ///     Console.WriteLine($"Result type {entry.Key} value is {entry.Value}");
    /// }
    /// </code>
    /// </example>
    [JsonConverter(typeof(SerpsSummaryEntryConverter))]
    public class SerpsSummaryEntry : Dictionary<string, long>
    {
        /// <value>Location value key in the SERPs API response object.</value>
        public const string LocationKey = "location";

        /// <value>Location of the search results.</value>
        public string Location { get; set; }

        /// <summary>
        /// Summary value accessor.
        /// </summary>
        /// <example>
        /// This sample shows how to access value by a search result type.
        /// <code>
        /// int count = response.Response.Summary.Global["organic"];
        /// </code>
        /// </example>
        public new long this[string key] => this.GetValueOrDefault(key);
    }
}
