using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Person search result enrichment.
    /// </summary>
    [DynamicObjectDiscriminator(PersonRichSnippet.DiscriminatorValue)]
    public class PersonRichSnippet : RichSnippet
    {
        /// <value>Value of the discriminator property for current rich snippet class.</value>
        public const string DiscriminatorValue = "person";

        /// <value>Main location of person or employer. Might not always be shown.</value>
        public string Location { get => GetProperty<string>("location"); }

        /// <value>Known role. Might not always be shown.</value>
        public string Position { get => GetProperty<string>("position"); }
    }
}
