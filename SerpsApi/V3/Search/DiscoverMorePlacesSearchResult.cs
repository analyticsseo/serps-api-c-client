using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Discover more places search result.
    /// </summary>
    [DynamicObjectDiscriminator(DiscoverMorePlacesSearchResult.DiscriminatorValue)]
    public class DiscoverMorePlacesSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "discover_more_places";

        /// <value>True if result is a carousel.</value>
        public bool? Carousel { get => GetProperty<bool?>("carousel"); }
    }
}
