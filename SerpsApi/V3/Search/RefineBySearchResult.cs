using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Refine by search result.
    /// </summary>
    [DynamicObjectDiscriminator(RefineBySearchResult.DiscriminatorValue)]
    public class RefineBySearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "refine_by";

        /// <value>True if it's refine by brand result.</value>
        public bool? Brand { get => GetProperty<bool?>("brand"); }

        /// <value>The refine by items - brands, products, product attributes, etc.</value>
        public Dictionary<string, string> Items { get => GetDictionary<string, string>("items"); }
    }
}
