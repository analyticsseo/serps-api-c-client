using Newtonsoft.Json;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Base class for building static-typed rich snippet classes.
    /// </summary>
    [JsonConverter(typeof(DynamicObjectConverter<RichSnippet, string>), RichSnippet.DiscriminatorProperty)]
    public class RichSnippet : DynamicObject
    {
        /// <value>Name of the property that determines rich snippet class.</value>
        public const string DiscriminatorProperty = "type";

        /// <value>Rich snippet type.</value>
        public new string Type { get => GetProperty<string>("type"); }
    }
}
