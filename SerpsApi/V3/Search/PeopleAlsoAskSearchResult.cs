using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Reople also ask search result.
    /// </summary>
    [DynamicObjectDiscriminator(PeopleAlsoAskSearchResult.DiscriminatorValue)]
    public class PeopleAlsoAskSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "people_also_ask";

        /// <value>The question.</value>
        public string Question { get => GetProperty<string>("question"); }

        /// <value>The answer.</value>
        public string Answer { get => GetProperty<string>("answer"); }
    }
}
