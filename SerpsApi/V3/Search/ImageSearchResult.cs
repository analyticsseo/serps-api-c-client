using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Image search result.
    /// </summary>
    [DynamicObjectDiscriminator(ImageSearchResult.DiscriminatorValue)]
    public class ImageSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "image";

        /// <value>The result position, left or right.</value>
        public string Position { get => GetProperty<string>("position"); }
    }
}
