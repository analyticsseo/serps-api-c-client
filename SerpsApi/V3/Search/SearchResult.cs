using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Represents a generic search result item.
    /// </summary>
    [JsonConverter(typeof(DynamicObjectConverter<SearchResult, string>), SearchResult.DiscriminatorProperty)]
    public class SearchResult : DynamicObject
    {
        /// <value>Name of the property that determines search result class.</value>
        public const string DiscriminatorProperty = "type";

        /// <value>
        /// The type of search result.
        /// Can be for example organic, image, video, etc.
        /// </value>
        public new string Type { get => GetProperty<string>("type"); }

        /// <value>The item title.</value>
        public string Title { get => GetProperty<string>("title"); }

        /// <value>The item description.</value>
        public string Description { get => GetProperty<string>("description"); }

        /// <value>The URL of the item.</value>
        public Uri Url
        {
            get
            {
                Uri result = null;

                var url = GetProperty<string>("url");
                if (url != null)
                {
                    Uri.TryCreate(url, UriKind.Absolute, out result);
                }

                return result;
            }
        }

        /// <value>The page number where the item was found.</value>
        public int? PageNumber { get => GetProperty<int?>("page_number"); }

        /// <value>A collection of rich snippets in form of static typed objects.</value>
        public IEnumerable<RichSnippet> RichSnippets
        {
            get
            {
                var snippets = this["rich_snippets"] as JArray;
                if (snippets != null)
                {
                    return snippets.Select(snippet => snippet as JObject)
                        .Where(snippet => snippet != null)
                        .Select(snippet => snippet.ToObject<RichSnippet>())
                        .ToList();
                }
                else
                {
                    return Enumerable.Empty<RichSnippet>();
                }
            }
        }

        /// <value>
        /// The version of the markup parsed.
        /// The same search result might be presented in different ways by the same search engine,
        /// this number represents our internal version of the parser used.
        /// </value>
        public int? Markup { get => GetProperty<int?>("markup"); }

        /// <value>The result's top left corner coordinates (pixels).</value>
        public string TopLeft { get => GetProperty<string>("top_left"); }

        /// <value>The result's bottom right corner coordinates (pixels).</value>
        public string BottomRight { get => GetProperty<string>("bottom_right"); }

        /// <value>
        /// Is the result visible on the page.
        /// All results apart from those requiring scrolling in a carousel are visible.
        /// </value>
        public bool? Visible { get => GetProperty<bool?>("visible"); }

        /// <value>
        /// Is the result immediately visible on page load.
        /// This will be true for results that do not require any scrolling to view.
        /// </value>
        public bool? AboveTheFold { get => GetProperty<bool?>("above_the_fold"); }
    }
}
