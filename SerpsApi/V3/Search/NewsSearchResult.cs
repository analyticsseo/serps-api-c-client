using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// News search result.
    /// </summary>
    [DynamicObjectDiscriminator(NewsSearchResult.DiscriminatorValue)]
    public class NewsSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "news";

        /// <value>True if result is an AMP page (Google accelerated mobile page). </value>
        public bool? Amp { get => GetProperty<bool?>("amp"); }

        /// <value>True if result is a carousel.</value>
        public bool? Carousel { get => GetProperty<bool?>("carousel"); }

        /// <value>True if result is a video.</value>
        public bool? Video { get => GetProperty<bool?>("video"); }
    }
}
