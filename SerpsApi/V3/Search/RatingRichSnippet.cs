using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Rating search result enrichment.
    /// </summary>
    [DynamicObjectDiscriminator(RatingRichSnippet.DiscriminatorValue)]
    public class RatingRichSnippet : RichSnippet
    {
        /// <value>Value of the discriminator property for current rich snippet class.</value>
        public const string DiscriminatorValue = "rating";

        /// <value>The rating score, might not always be shown.</value>
        public decimal? Score { get => GetProperty<decimal?>("score"); }

        /// <value>The number of votes, might not always be shown.</value>
        public int? Votes { get => GetProperty<int?>("votes"); }

        /// <value>The raw snippet, exactly as present in the search result.</value>
        public string Raw { get => GetProperty<string>("raw"); }

        /// <value>The entity making a review, might not always be shown.</value>
        public string Reviewer { get => GetProperty<string>("reviewer"); }

        /// <value>The date, might not always be shown.</value>
        public string Date { get => GetProperty<string>("date"); }

        /// <value>The time. For example, cooking time, might not always be shown</value>
        public string Time { get => GetProperty<string>("time"); }

        /// <value>The price. It can also be a price range, might not always be shown.</value>
        public string Price { get => GetProperty<string>("price"); }

        /// <value>The type of rating. Based on what we can extract we will try to guess the type of item being rated.</value>
        public string GuessedType { get => GetProperty<string>("guessed_type"); }
    }
}
