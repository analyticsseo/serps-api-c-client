using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Advertisement search result.
    /// </summary>
    [DynamicObjectDiscriminator(AdvertisementSearchResult.DiscriminatorValue)]
    public class AdvertisementSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "ad";

        /// <value>
        /// True is result is on top of SERPS.
        /// On top means that it appears before any other type of search results.
        /// </value>
        /// <remarks>
        /// Note: some markups might not link directly to a landing site and instead use a redirection link to the ad network,
        /// in most cases an "user friendly" landing URL is displayed with the ad and it will be used instead of the ad network
        /// URL That "user friendly" URL might not contain the protocol (e.g: www.foo.bar instead of http://www.foo.bar)
        /// </remarks>
        public bool? OnTop { get => GetProperty<bool?>("on_top"); }
    }
}
