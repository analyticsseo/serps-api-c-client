using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Job finder search result.
    /// </summary>
    [DynamicObjectDiscriminator(JobFinderSearchResult.DiscriminatorValue)]
    public class JobFinderSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "job_finder";

        /// <value>Job items.</value>
        public Dictionary<string, string> Items { get => GetDictionary<string, string>("items"); }

        /// <value>Job filters.</value>
        public Dictionary<string, string> Filters { get => GetDictionary<string, string>("filters"); }
    }
}
