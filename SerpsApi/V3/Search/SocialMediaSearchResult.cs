using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Social media search result.
    /// </summary>
    [DynamicObjectDiscriminator(SocialMediaSearchResult.DiscriminatorValue)]
    public class SocialMediaSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "social_media";

        /// <value>The type of social media search result.</value>
        public string SubType { get => GetProperty<string>("sub_type"); }

        /// <value>True if result is a carousel.</value>
        public bool? Carousel { get => GetProperty<bool?>("carousel"); }

        /// <value>True if result is a video.</value>
        public bool? Video { get => GetProperty<bool?>("video"); }
    }
}
