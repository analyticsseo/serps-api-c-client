using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// People also search for search result.
    /// </summary>
    [DynamicObjectDiscriminator(PeopleAlsoSearchForSearchResult.DiscriminatorValue)]
    public class PeopleAlsoSearchForSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "people_also_search_for";

        /// <value>The phrases people also search for.</value>
        public Dictionary<string, string> Phrases { get => GetDictionary<string, string>("phrases"); }
    }
}
