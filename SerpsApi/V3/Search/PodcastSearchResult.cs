using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Podcast search result.
    /// </summary>
    [DynamicObjectDiscriminator(PodcastSearchResult.DiscriminatorValue)]
    public class PodcastSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "podcast";

        /// <value>True if result is a carousel.</value>
        public bool? Carousel { get => GetProperty<bool?>("carousel"); }

        /// <value>The age of podcast.</value>
        public string Age { get => GetProperty<string>("age"); }

        /// <value>The length of podcast.</value>
        public string Length { get => GetProperty<string>("length"); }
    }
}
