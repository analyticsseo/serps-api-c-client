using System;
using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
// Hi Nic, any news on the library review? Or you need more time to check the repo?

    /// <summary>
    /// Knowledge graph search result.
    /// </summary>
    [DynamicObjectDiscriminator(KnowledgeGraphSearchResult.DiscriminatorValue)]
    public class KnowledgeGraphSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "knowledge_graph";

        /// <summary>Item entry for <c>KnowledgeGraphSearchResult</c>.</summary>
        [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
        public class Item
        {
            /// <value>The item category.</value>
            public string Category { get; set; }

            /// <value>The item label.</value>
            public string Label { get; set; }

            /// <value>The item value.</value>
            public string Value { get; set; }
        }

        /// <value>The optional url to connected resource.</value>
        public string EntityUrl { get => GetProperty<string>("entity_url"); }

        /// <value>The result position, left or right.</value>
        public string Position { get => GetProperty<string>("position"); }

        /// <value>The additional sub title.</value>
        public string SubTitle { get => GetProperty<string>("sub_title"); }

        /// <value>The type of knowledge graph, local_business_listing, book, review, sport or other.</value>
        public string SubType { get => GetProperty<string>("sub_type"); }

        /// <value>Additional information like address, reviews.</value>
        public Dictionary<string, Item> OtherItems { get => GetDictionary<string, Item>("other_items"); }

        /// <value>The social profiles - facebook, twitter, etc. profile urls.</value>
        public Dictionary<string, Uri> Profiles { get => GetDictionary<string, Uri>("profiles"); }
    }
}
