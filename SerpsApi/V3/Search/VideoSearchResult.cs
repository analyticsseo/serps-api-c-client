using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Video search result.
    /// </summary>
    [DynamicObjectDiscriminator(VideoSearchResult.DiscriminatorValue)]
    public class VideoSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "video";

        /// <value>
        /// True is result is featured and on top.
        /// Featured means that there is a larger preview and the result is on top.
        /// </value>
        public bool? Featured { get => GetProperty<bool?>("featured"); }
    }
}
