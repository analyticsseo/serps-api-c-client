using System.Collections.Generic;
using Newtonsoft.Json;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// A container class for ranked search results collection.
    /// The key in the dictionary - is the position (ranking) of the search result.
    /// </summary>
    [JsonConverter(typeof(SearchResultDictionaryConverter))]
    public class SearchResultDictionary : Dictionary<string, SearchResult> { }
}
