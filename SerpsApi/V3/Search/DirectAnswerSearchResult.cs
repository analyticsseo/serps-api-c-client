using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Direct answer search result.
    /// </summary>
    [DynamicObjectDiscriminator(DirectAnswerSearchResult.DiscriminatorValue)]
    public class DirectAnswerSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "direct_answer";

        /// <value>
        /// The type of Direct Answer.
        /// Might be paragraph, list, table, currency_converter, translation, stock price, dictionary or other.
        /// </value>
        public string SubType { get => GetProperty<string>("sub_type"); }

        /// <value>
        /// The title of Direct Answer.
        /// Only filled if title is present in the result.
        /// </value>
        public string Header { get => GetProperty<string>("header"); }

        /// <value>
        /// The steps detected on the list.
        /// Direct answer of sub_type list contains "steps" (ordered list of entries), those are stored here.
        ///</value>
        public Dictionary<string, string> Steps { get => GetDictionary<string, string>("steps"); }
    }
}
