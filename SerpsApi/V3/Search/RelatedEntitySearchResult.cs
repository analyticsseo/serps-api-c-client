using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Related entity search result.
    /// </summary>
    [DynamicObjectDiscriminator(RelatedEntitySearchResult.DiscriminatorValue)]
    public class RelatedEntitySearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "related_entity";

        /// <value>The related search entities.</value>
        public Dictionary<string, string> Items { get => GetDictionary<string, string>("items"); }
    }
}
