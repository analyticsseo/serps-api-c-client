using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Research guide search result.
    /// </summary>
    [DynamicObjectDiscriminator(ResearchGuideSearchResult.DiscriminatorValue)]
    public class ResearchGuideSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "research_guide";

        /// <value>True if result is an AMP page (Google accelerated mobile page). </value>
        public bool? Amp { get => GetProperty<bool?>("amp"); }

        /// <value>True if result is a carousel.</value>
        public bool? Carousel { get => GetProperty<bool?>("carousel"); }
    }
}
