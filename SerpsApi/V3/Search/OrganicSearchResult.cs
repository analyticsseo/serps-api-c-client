using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Organic search result.
    /// </summary>
    [DynamicObjectDiscriminator(OrganicSearchResult.DiscriminatorValue)]
    public class OrganicSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "organic";

        /// <value>True if result is an AMP page (Google accelerated mobile page). </value>
        public bool? Amp { get => GetProperty<bool?>("amp"); }

        /// <value>True if result is a carousel.</value>
        public bool? Carousel { get => GetProperty<bool?>("carousel"); }

        /// <value>True if result contains FAQ.</value>
        public bool? Faq { get => GetProperty<bool?>("faq"); }

        /// <value>True if result contains thumbnail.</value>
        public bool? Thumbnail { get => GetProperty<bool?>("thumbnail"); }

        /// <value>True if result contains video thumbnail.</value>
        public bool? VideoThumbnail { get => GetProperty<bool?>("video_thumbnail"); }
    }
}
