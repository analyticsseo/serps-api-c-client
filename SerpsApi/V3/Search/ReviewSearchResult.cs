using System;
using System.Collections.Generic;
using Authoritas.SerpsApi.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Review search result.
    /// </summary>
    [DynamicObjectDiscriminator(ReviewSearchResult.DiscriminatorValue)]
    public class ReviewSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "review";

        /// <summary>Item entry for <c>KnowledgeGraphSearchResult</c>.</summary>
        [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
        public class Item
        {
            /// <value>The item name.</value>
            public string Name { get; set; }

            /// <value>The review.</value>
            public string Review { get; set; }

            /// <value>The review URL.</value>
            public Uri Url { get; set; }

            /// <value>Is review visible.</value>
            public bool Visible { get; set; }
        }

        /// <value>The reviews items.</value>
        public Dictionary<string, Item> Items { get => GetDictionary<string, Item>("items"); }
    }
}
