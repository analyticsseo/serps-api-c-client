using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Site links search result enrichment.
    /// </summary>
    [DynamicObjectDiscriminator(SiteLinksRichSnippet.DiscriminatorValue)]
    public class SiteLinksRichSnippet : RichSnippet
    {
        /// <value>Value of the discriminator property for current rich snippet class.</value>
        public const string DiscriminatorValue = "site_links";

        /// <summary>Link entry for <c>SiteLinksRichSnippet</c>.</summary>
        [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
        public class Link
        {
            /// <value>The link url.</value>
            public Uri Url { get; set; }

            /// <value>The link title.</value>
            public string Title { get; set; }

            /// <value>The link description.</value>
            public string Description { get; set; }
        }

        /// <value>Collection of link entries.</value>
        public IEnumerable<Link> Links
        {
            get
            {
                var links = this["links"] as JArray;
                if (links != null)
                {
                    return links.Select(link => link.ToObject<Link>())
                        .Where(link => link != null)
                        .ToList();
                }
                else
                {
                    return Enumerable.Empty<Link>();
                }
            }
        }
    }
}
