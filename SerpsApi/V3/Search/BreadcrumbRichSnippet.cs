using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Breadcrumb search result enrichment.
    /// </summary>
    [DynamicObjectDiscriminator(BreadcrumbRichSnippet.DiscriminatorValue)]
    public class BreadcrumbRichSnippet : RichSnippet
    {
        /// <value>Value of the discriminator property for current rich snippet class.</value>
        public const string DiscriminatorValue = "breadcrumb";

        /// <value>
        /// The breadcrumb path shown.
        /// This might not be the full breadcrumb, it is just what the search engine shows.
        /// </value>
        public new string Path { get => GetProperty<string>("path"); }
    }
}
