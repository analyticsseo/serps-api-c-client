using Authoritas.SerpsApi.Serialization;

namespace Authoritas.SerpsApi.V3.Search
{
    /// <summary>
    /// Shopping item search result.
    /// </summary>
    [DynamicObjectDiscriminator(ShoppingItemSearchResult.DiscriminatorValue)]
    public class ShoppingItemSearchResult : SearchResult
    {
        /// <value>Value of the discriminator property for current search result class.</value>
        public const string DiscriminatorValue = "shopping";

        /// <value>The merchant name.</value>
        public string Merchant { get => GetProperty<string>("merchant"); }

        /// <value>The rating value.</value>
        public decimal? Rating { get => GetProperty<decimal?>("rating"); }

        /// <value>The result position, left or right.</value>
        public string Position { get => GetProperty<string>("position"); }

        /// <value>The additional information.</value>
        public string Info { get => GetProperty<string>("info"); }

        /// <value>True if it's a Google shopping offer.</value>
        public bool? ShopFor { get => GetProperty<bool?>("shop_for"); }
    }
}
