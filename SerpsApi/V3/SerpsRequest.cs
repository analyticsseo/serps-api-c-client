using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Authoritas.SerpsApi.V3
{
    /// <summary>
    /// SERPs API request object.
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public class SerpsRequest
    {
        /// <value>The search engine to query.</value>
        public string SearchEngine { get; set; } = string.Empty;

        /// <value>The search term / keyword.</value>
        public string Phrase { get; set; } = string.Empty;

        /// <value>The region code to use.</value>
        public string Region { get; set; } = string.Empty;

        /// <value>The language code to use.</value>
        public string Language { get; set; } = string.Empty;

        /// <value>The town to use.</value>
        public string Town { get; set; }

        /// <value>The latitude of the location to geolocate results to.</value>
        public string Latitude { get; set; }

        /// <value>The longitude of the location to geolocate results to.</value>
        public string Longitude { get; set; }

        /// <value>The number of results to return.</value>
        public int MaxResults { get; set; } = 100;

        /// <value>The user agent to use.</value>
        public string UserAgent { get; set; } = Authoritas.SerpsApi.V3.UserAgent.PC;

        /// <value>Whether to use cached data, if available.</value>
        public bool UseCache { get; set; } = true;

        /// <value>Whether to incude ads in universal section.</value>
        public bool IncludeAllInUniversal { get; set; } = false;

        /// <value>The url to call after request is ready.</value>
        public Uri Callback { get; set; }
    }
}
