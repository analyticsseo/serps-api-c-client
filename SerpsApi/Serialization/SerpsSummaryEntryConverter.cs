using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Authoritas.SerpsApi.V3;

namespace Authoritas.SerpsApi.Serialization
{
    /// <summary>
    /// Custom JSON serializer for <c>SerpsSummaryEntry</c>.
    /// </summary>
    public class SerpsSummaryEntryConverter : JsonConverter<SerpsSummaryEntry>
    {
        public override SerpsSummaryEntry ReadJson(JsonReader reader, Type objectType, SerpsSummaryEntry existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var summary = new SerpsSummaryEntry();
            if (reader.TokenType != JsonToken.StartObject)
            {
                throw new JsonException("SerpsSummaryEntry is not a valid object");
            }

            var item = JObject.Load(reader);

            // Storing location value
            summary.Location = (string) (item[SerpsSummaryEntry.LocationKey] as JValue);

            // Storing integer values into the dictionary
            foreach (var property in item.Properties())
            {
                if (property.Value.Type == JTokenType.Integer)
                {
                    summary.Add(property.Name, property.Value.Value<long>());
                }
            }

            return summary;
        }

        public override void WriteJson(JsonWriter writer, SerpsSummaryEntry value, JsonSerializer serializer)
        {
            writer.WriteStartObject();

            // Writing location value
            if (value.Location != null)
            {
                writer.WritePropertyName(SerpsSummaryEntry.LocationKey);
                writer.WriteValue(value.Location);
            }

            // Writing integer values from the dictionary
            foreach (var pair in value)
            {
                writer.WritePropertyName(pair.Key);
                writer.WriteValue(pair.Value);
            }

            writer.WriteEndObject();
        }
    }
}
