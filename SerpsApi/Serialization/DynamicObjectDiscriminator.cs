using System;

namespace Authoritas.SerpsApi.Serialization
{
    /// <summary>Attribute to set dynamic object's sub-class discriminator value.</summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DynamicObjectDiscriminator : Attribute
    {
        /// <value>Discriminator property value.</value>
        public object Discriminator { get; }

        /// <summary>Constructor for <c>DynamicObjectDiscriminator</c>.</summary>
        public DynamicObjectDiscriminator(object discriminator)
        {
            Discriminator = discriminator;
        }
    }
}
