using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Authoritas.SerpsApi.V3.Search;

namespace Authoritas.SerpsApi.Serialization
{
    /// <summary>
    /// A container class for ranked search results collection.
    /// </summary>
    public class SearchResultDictionaryConverter : JsonConverter<SearchResultDictionary>
    {
        public override SearchResultDictionary ReadJson(JsonReader reader, Type objectType, SearchResultDictionary existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var results = new SearchResultDictionary();

            var data = JToken.Load(reader) as JObject;
            if (data != null)
            {
                foreach (var prop in data.Properties())
                {
                    if (prop.Value is JObject)
                    {
                        // serializer.Deserialize<SearchResult>(prop.Value)
                        results.Add(prop.Name, prop.Value.ToObject<SearchResult>());
                    }
                }
            }

            return results;
        }

        public override void WriteJson(JsonWriter writer, SearchResultDictionary value, JsonSerializer serializer)
        {
            writer.WriteStartObject();
            foreach (var pair in value)
            {
                if (pair.Value != null)
                {
                    writer.WritePropertyName(pair.Key);
                    serializer.Serialize(writer, pair.Value);
                }
            }

            writer.WriteEndObject();
        }
    }
}
