using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Authoritas.SerpsApi.Serialization
{
    /// <summary>
    /// Base class for constructing dynamic objects based on <c>JObject</c>.
    /// </summary>
    public class DynamicObject : JObject
    {
        /// <summary>
        /// Gets a type-casted property value by a key.
        /// </summary>
        /// <param name="key">Property key.</param>
        /// <example>
        /// Compare usage with dictionary syntax:
        /// <code>
        /// var a = result.GetProperty<string>("title"); // OK, if "title" is not present - null will be returned
        /// var b = result["title"].Value<string<(); // NullReferenceException will be thrown if "title" property is missing
        /// </code>
        /// </example>
        public T GetProperty<T>(string key)
        {
            JToken token;
            if (TryGetValue(key, StringComparison.CurrentCulture, out token))
            {
                try
                {
                    return token.Value<T>();
                }
                catch (Exception) { }
            }

            return default(T);
        }

        /// <summary>Extracts a static-typed dictionary from object property by a key.</summary>
        public Dictionary<K, V> GetDictionary<K, V>(string key)
        {
            var obj = this[key] as JObject;
            if (obj != null)
            {
                return obj.ToObject<Dictionary<K, V>>();
            }
            else
            {
                return null;
            }
        }
    }
}
