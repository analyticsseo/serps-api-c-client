using System;
using System.Threading;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Authoritas.SerpsApi.Serialization
{
    /// <summary>
    /// Custom JSON converter for <c>DynamicObject</c>.
    /// Deserializes JSON data to a specific sub-class object based on the discriminator property value.
    /// </summary>
    public class DynamicObjectConverter<TObjectType, TDiscriminatorType> : JsonConverter where TObjectType : DynamicObject
    {
        public const string InvalidJsonExceptionMessage = "Dynamic object source must be a valid JSON object";

        private string discriminatorProperty;

        private static Dictionary<string, Dictionary<TDiscriminatorType, Type>> typeVariantMapCache = new Dictionary<string, Dictionary<TDiscriminatorType, Type>>();

        private static Mutex typeVariantMapMutex = new Mutex();

        /// <summary>Constructor for <c>DynamicObjectConverter</c>.</summary>
        /// <param name="discriminatorProperty">Discriminator property name.</param>
        public DynamicObjectConverter(string discriminatorProperty)
        {
            this.discriminatorProperty = discriminatorProperty;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(TObjectType).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.StartObject)
            {
                throw new JsonException(InvalidJsonExceptionMessage);
            }

            var data = JObject.Load(reader);
            var discriminator = GetDiscriminatorValue(data);

            Type resolvedType = null;
            if (discriminator != null)
            {
                var typeVariantMap = GetTypeVariantMap(objectType);
                typeVariantMap.TryGetValue(discriminator, out resolvedType);
            }

            // Create instance of a specified sub-class
            var instance = Activator.CreateInstance(resolvedType ?? typeof(TObjectType)) as TObjectType;

            // Populate created instance with properties from the original data object
            foreach (var prop in data.Properties())
            {
                instance.Add(prop.Name, prop.Value);
            }

            return instance;
        }

        private TDiscriminatorType GetDiscriminatorValue(JObject data)
        {
            var discriminatorProperty = data[this.discriminatorProperty];
            if (discriminatorProperty == null)
            {
                return default(TDiscriminatorType);
            }

            try
            {
                return discriminatorProperty.Value<TDiscriminatorType>();
            }
            catch (Exception)
            {
                return default(TDiscriminatorType);
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            (value as TObjectType).WriteTo(writer);
        }

        private Dictionary<TDiscriminatorType, Type> GetTypeVariantMap(Type baseType)
        {
            typeVariantMapMutex.WaitOne();
            try
            {
                if (typeVariantMapCache.ContainsKey(baseType.FullName))
                {
                    return typeVariantMapCache[baseType.FullName];
                }

                var subTypes = Assembly.GetAssembly(baseType).GetTypes()
                    .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(baseType));

                var typeVariantMap = new Dictionary<TDiscriminatorType, Type>();
                foreach (var subType in subTypes)
                {
                    var attr = subType.GetCustomAttribute<DynamicObjectDiscriminator>();
                    if (attr != null)
                    {
                        typeVariantMap.Add((TDiscriminatorType)attr.Discriminator, subType);
                    }
                }

                typeVariantMapCache.TryAdd(baseType.FullName, typeVariantMap);
                return typeVariantMap;
            }
            finally
            {
                typeVariantMapMutex.ReleaseMutex();
            }
        }
    }
}
